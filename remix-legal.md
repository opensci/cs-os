# Reuse, remix, and legal considerations

## Introduction to legal concepts

A core principle of open science is facilitating the iterative cycle of science, where we have the freedom to build on what came before. In the words of the UNESCO Recommendation on Open Science, open science should make “scientific knowledge openly available, accessible and reusable for everyone…” Unfortunately, the legal environment we live in (which is generally true for most of the world) aggressively restricts the flow of knowledge under an “all rights reserved” assumption. In other words, by default, any form of knowledge or creative work cannot be shared or reused. These legal restrictions sometimes creep up in surprisingly trivial or mundane situations.

This section will start with a brief primer on this restrictive legal environment. Then, we will introduce simple ways to ensure your works will be shareable and easily accessible by others.

**Disclaimer:** This section will introduce some general legal concepts. However, no content or statement in this course, explicit or implied, should be construed as professional legal advice. When in doubt, please consult a legal professional appropriate for your jurisdiction.

## Quick legal primer

For the majority of open science outputs, the most relevant legal instrument is copyright. Even though the specifics of copyright law vary between countries, bear in mind the following general concepts:

1. Copyright automatically applies as soon as you express a creative work and “fix” it in any medium. This is very broadly defined and could be a scientific dataset, academic paper, or seemingly trivial expressions like social media posts, emails, and even scribbles on a napkin.
2. The author of a work is usually the copyright holder, but copyright can be transferred to any other person or organisation.
3. The copyright holder has the exclusive privilege of dictating the terms of how their copyrighted work can or cannot be used by others.

These concepts can feel abstract, so let us look at two famous cases to help understand them.

### Happy birthday

The [Happy Birthday song](https://en.wikipedia.org/wiki/Happy_Birthday_to_You#Lyrics) is considered one of the most popular songs in the world and has been translated into at least 18 languages [1]. You might have heard it being sung during one of your, or someone else’s, birthday. In case you are not familiar with the Happy Birthday song, this is its melody: 

> Happy Birthday instrumental arrangement by Eirik1231 [from Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Good_Morning_to_All.ogg), dedicated to the public domain

What is less known about this song is its complicated relationship with copyright law.

The first known publication of Happy Birthday’s melody and words dates to 1912 in the United States. The copyright to this song was held by The Summy Company who cited Preston Ware Orem and R. R. Forman as the authors. In 1988, Warner/Chappel Music bought the copyright to Happy Birthday. Observe that this is an example where the author is not the copyright holder of a work, which is often true in the music industry. We can also see the transfer of copyright from one entity to another, in this case between two music publishers. Even in science, some academic journals require a paper’s author to transfer their copyright to the publisher.

As the copyright holder to Happy Birthday, The Summy Company, and later Warner/Chappel Music, held exclusive control over what other people can do with this song. Technically, it was a criminal offence for you to sing this song in public (a “public performance”) without express permission from the music company! When Warner/Chappel Music gave someone permission to perform Happy Birthday, that is called a “license”. Simply put, a license is the set of permissions and conditions a copyright holder gives to another person to share or make use of their work. To get a license from Warner/Chappel Music to perform Happy Birthday, you had to pay at least $700 USD for a single “use” of the song. That is why you rarely see Happy Birthday performed in film or television. To save money and paperwork, script writers simply found some other way for characters to celebrate a birthday.

Throughout the 20th century, copyright law was revised many times in the United States, and the complex history of Happy Birthday’s copyright transfers caused much legal dispute. In 2016, it was finally determined that the copyright to the Happy Birthday song had expired in the United States, more than a century after its first known publication. The copyright to this song expired in the European Union in 2017. You can now confidently perform this song without permission or fear of imprisonment because it is finally in the public domain (at least in the US and EU).

### Dancing baby

Dancing babies don’t usually get people into trouble. But for Stephanie Lenz, sharing a 29-second video of her toddler led to a decade-long legal dispute now known as the “Dancing Baby” case [2]. Soon after posting this video to YouTube in 2007, Stephanie received a copyright complaint from Universal Music. Apparently, her child was dancing to a song called “Let’s Go Crazy” which happened to be playing in the background. By allegedly remixing the song into the video, Stephanie was accused of committing copyright infringement. In the worst case, she could be sent to jail.

In the United States and several other jurisdictions, there is a legal right called “fair use” (or “fair dealing” in some countries) which allows the use of copyrighted works without a license. Fair use is possible under a narrow set of conditions, such as in certain educational settings, highly “transformational” uses, parody or critique, or relatively mundane situations like Stephanie’s dancing baby.

Unfortunately, what counts as fair use is highly subjective, and it varies greatly between different jurisdictions. In Stephanie’s case, it took more than 10 years of costly litigation to establish her fuzzy 29-second dancing baby video as fair use, and that it did not cause major damages to an international music conglomerate. She can finally share this video of her son, who is now attending high school.

If you can spare 29 seconds, take a look at Stephanie’s original video below (can you recognise the song playing in the background?).

We believe the inclusion of this video here is fair use. But if Stephanie sends us a copyright complaint, we will remove it. We can’t afford the costs of litigation!

> Video of a dancing baby by Stephanie Lenz [from YouTube](https://www.youtube.com/watch?v=N1KfJHFWlhQ) and [archived at the Internet Archive](https://archive.org/details/lets-go-crazy-n1), fair use

### References

1. Brauneis, R., 2010. Copyright and the world’s most popular song. Journal of the Copyright Society of the U.S.A., GWU Legal Studies Research Paper No. 392. https://doi.org/10.2139/ssrn.1111624

2. Electronic Frontier Foundation, 2011. Lenz v. Universal [WWW Document]. Electronic Frontier Foundation. URL https://www.eff.org/cases/lenz-v-universal

## Why should I care about copyright?

One might say: “Unlike Stephanie, the outputs from my project don't include copyrighted songs, why should I care?” Or “I’ve published blog posts about my citizen science project, uploaded our data to Zenodo, and shared the source code of our mobile app on GitHub. I’m happy for others to make use of them, isn’t that enough?” Unfortunately, **no**.

![Lightbulb in chains to signify copyright's chilling effect on creativity](media/Ideas_in_chains.svg.png)

> Ideas in chains by Pen-Yuan Hsing, [from Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Ideas_in_chains.svg), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en)

*Remember, copyright automatically restricts your work as soon as you create it. You can’t even refuse it!* As we have seen from the case studies, it is a criminal offence to make copies of (let alone changes to) a work without a license from the copyright holder. If a teacher prints copies of your blog post for their students; someone downloads a copy of your data to analyse; or a developer tweaks your code to add improvements, those are all illegal by default. And as we learned from Stephanie, it is risky to rely on fair use rights. Even if you believe your unlicensed use is fair, someone else might disagree. For better or worse, everyone has to request a license for every single use of a copyrighted work.

Another way to think about this is when you’re preparing a presentation about your citizen science project. You’re the copyright holder of photos you took or text you wrote, but when you find material on the Internet to help spruce up your slides, do they come with clear licenses? If not, then you are technically committing copyright infringement, a major criminal offence. The threat of copyright litigation has a chilling effect on creativity in the arts and sciences, which is explored in the book [Free Culture by Lawrence Lessig](https://lessig.org/product/free-culture) [1], a law professor at Harvard University.

While copyright technicalities might seem convoluted and tricky, fortunately, there are tools to help us make sharing, reusing, and remixing open science outputs very easy. We will cover this next.

### References

1. Lessig, L., 2005. Free culture: The nature and future of creativity. Penguin Books.

## Why do we need open licenses? An example.

Let's illustrate the need for open licenses via an example.

You run a citizen science project for air quality monitoring. Today, the city council passed an ordinance on stricter air quality controls, which was informed by the data your project collected! You excitedly publish an update to the project blog announcing this development. The text you just published is automatically restricted by copyright, with you as the copyright holder. By default, no one else can make use of it in whole or part.

Later, you receive an email from the editor of a popular science magazine. They would like to reprint part of your post in an upcoming article about citizen science and public policy. You agree. Generally speaking, you have just granted the magazine a license to use your work. In the eyes of the law, you are saying: “With the power vested in me by copyright law, I hereby grant this magazine a license to use the text of my blog post in their upcoming article.” As we have seen in previous sections, since sharing and reuse can only happen when the copyright holder grants a license allowing it, there are several problems that make open science difficult as we will discuss next.

With the publicity from the magazine article, many more citizen scientists have joined your project to monitor air quality! One of them tells you that there is a similar project in a nearby city also collecting air quality data. You realise that if you combine their dataset with yours, you can analyse trends in air quality for the entire region. The other project has kindly posted their data on their website. However, there is no license for this data on the webpage, and the project owner still hasn’t responded to your email asking for permission to use the data. Therefore, while the data is available, you can't use it!

The ad-hoc process of seeking and obtaining a license for every use of copyrighted material is time-consuming and frustrating. It is often hard to identify, let alone track down each copyright holder. And when you do find contact information, such as an email, it might be out of date. And even if you successfully email them, the copyright holder might be on vacation and not get back to you in time. When they finally respond, they might tell you that the copyright belongs to someone else. You end up repeating the steps again.

## Use open licenses to make sharing and reuse easier

Copyright restrictions often create friction in the flow of knowledge, especially for open science processes. To prevent problems such as those in the example we discussed, legal experts, open science advocates, and those wishing for easier sharing of culture collaborated to write a set of standardised licenses that anyone can use.

![Share more with Creative Commons animation](media/Share_More_with_Creative_Commons.gif)

> Animated Creative Commons logo by Abbey Elder [from Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Share_More_with_Creative_Commons.gif), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

This effort was organised by a non-profit organisation called [Creative Commons](https://creativecommons.org/about/). Starting in 2002, a series of [six Creative Commons licenses were](https://creativecommons.org/about/cclicenses/) created. Each one allows the licensed work to be copied and shared, but with varying sets of conditions, some more restrictive than others. Creative Commons has a [step-by-step tool](https://creativecommons.org/share-your-work/) to help you choose and apply a license. They even have a [search engine](https://search.creativecommons.org/) for finding material that uses Creative Commons licenses.

In the spirit of open science as described in the UNESCO Recommendation on Open Science, two of the six Creative Commons licenses are generally used.

![CC BY logo](media/by.png) ![CC BY-SA logo](media/by-sa.png)

> Creative Commons license buttons by Creative Commons [from here](https://creativecommons.org/about/downloads/), [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

* The [Creative Commons Attribution license (CC BY)](https://creativecommons.org/licenses/by/4.0/) is the simplest to understand and use. Any work shared under CC BY can be copied and used by anyone for any purpose. The only condition is that proper attribution is provided to the original creator (hence the use of the word “Attribution” in the name of the license).
* The [Creative Commons Attribution-ShareAlike license (CC BY-SA)](https://creativecommons.org/licenses/by-sa/4.0/) is almost identical to CC BY but it comes with one extra condition: Derivative works must be shared under the same license.

The conditions of these two Creative Commons licenses may seem abstract, so let’s apply it to the previous example.

You just wrote a blog post announcing the new ordinance on air quality controls. This time, you attach the Creative Commons Attribution license to your blog post. It’s easy to do: just add a sentence stating “This blog post is shared under the Creative Commons Attribution license” and a link to the latest version of the license: https://creativecommons.org/licenses/by/4.0/

The editor of the popular science magazine notices your blog post. This time, they don’t need to contact you for permission to use your blog post in their article. You have already given permission by including the CC BY license. As long as the magazine abides by the one condition of the CC BY license, that is, by providing attribution to you, they can make full use of your blog post.

When you discover the air quality dataset collected from the nearby city, their website states that the data is released under the Creative Commons Attribution-ShareAlike license (CC BY-SA). This time, you don’t need to track down the creator of this data for permission to use it in your analysis, saving you a lot of time and effort. By combining their data with those from your local citizen science project, you successfully create an air quality map for the entire region. To fulfil the condition of the CC BY-SA license, you provide attribution to the original data collectors. And since CC BY-SA requires derivative works to be shared under the same license, you publish your combined dataset under CC BY-SA as well. One benefit of CC BY-SA is “paying it forward”: That means subsequent, derivative uses of an original work - such as a dataset - are required to maintain the same level of openness as the original.

It is easy to apply an open license to any product from your citizen science and open science research. In addition to what was described in the example, if you publish to platforms such as Zenodo, OSF, GitHub, or the Wikimedia Commons, their submission forms will ask you which license you want to attach to your work.

## Examples in citizen science

Let’s look at some examples of how easy it is to include licensing information with outputs from your project. Remember, when you attach an open license to your citizen science output, it is not just a way to reduce friction in the sharing and reuse of knowledge, but also to communicate your commitment to open science.

### Blog posts

Dr Alison Parker of the Wilson Center is an open science and citizen science researcher. She recently wrote [a great blog post on open science](https://www.wilsoncenter.org/blog-post/open-science-and-intellectual-property-using-open-licenses-open-your-science) and the use of open licenses (which you should read).

Right at the beginning, she simply added this sentence to apply the CC BY license to her article:

"This work is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)."

Can you find it?

### Snapshot Safari

As we discussed in section 2, the Snapshot Safari citizen science project has published their open educational materials for others to build and improve upon. Look at [that webpage](https://conservancy.umn.edu/handle/11299/217102) again.

This resource is shared under the Creative Commons Attribution license (CC BY), specifically version 4.0. Can you see where they indicated this?

### iNaturalist

The following photo of an endangered dragonfly was taken by a citizen scientist who contributed to the [iNaturalist](https://www.inaturalist.org/) project.

View its [original source page](https://www.inaturalist.org/photos/46804589), can you find where it states that this photo is shared under the CC BY 4.0 license?

![iNaturalist photo 46804589](media/file-614b33a0b87ee.jpeg)

> Photo by [Amila P. Sumanapala](https://www.inaturalist.org/users/56488) on [iNaturalist](https://www.inaturalist.org/photos/46804589). [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.en).

### Public Lab

Public Lab is a citizen science network running environmental monitoring projects across the world. [Publiclab.org](https://publiclab.org/) contains a wealth of knowledge with descriptions, methodologies, open source software and hardware, and many other forms of media related to each project.

If you scroll to the bottom of the homepage, you will see that the contents of the Public Lab website are: 

"...licensed under a [Creative Commons Attribution-ShareAlike 3.0 Unported License](https://creativecommons.org/licenses/by-sa/3.0/). Hardware designs on this site are released under the [CERN Open Hardware License 1.2](https://publiclab.org/ohl). Read more about Public Lab’s [open source licensing here](https://publiclab.org/licenses)."

Not only is their collective knowledge documented very well, but the clear licensing information on the Public Lab website makes it easy for others to learn from and build on that knowledge.

## Do open licenses mean all uses of my work must be non-commercial?

This is a frequently asked question about using open licenses such as the Creative Commons licenses.

Indeed, three of [the six Creative Commons licenses](https://creativecommons.org/about/cclicenses/) have a "non-commercial" clause (can you name which three?). This means if you apply these licenses to your published work, others are not allowed to use it for any commercial purpose.

However, a core value of open science is the freedom for others to share and reuse scientific outputs in any way for any purpose, including ones that make some money. This is why we have focused on the [CC BY](https://creativecommons.org/licenses/by/4.0/) and [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) licenses. These are the two that are compatible with open science principles which you should use.

Another complication is that what counts as "commercial purpose" is not well-defined and can vary dramatically between jurisdictions. For example, if your citizen science project shares its outputs under the [Creative Commons Attribution-NonCommercial license (CC BY-NC)](https://creativecommons.org/licenses/by-nc/4.0/), teachers from some countries won't be able to use it for teaching because that might be a "commercial" activity! The ambiguity in what "commercial" means causes a chilling effect on sharing and reuse for works licensed this way.

## A note on software and hardware licenses

There are specialised licenses designed for publishing open source software code and hardware designs. These licenses are conceptually equivalent to the CC BY and CC BY-SA licenses described earlier, but are written to address technical aspects unique to software and hardware. Without going into details here, the website  [choosealicense.com](https://choosealicense.com/) helps you apply an open source license to software. For hardware, the main “go-to” license is one of the [CERN Open Hardware Licenses](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2). The European Union-funded [Open!Next](https://opennext.eu/) project has produced a [short explainer of open source licenses for hardware](https://github.com/OPEN-NEXT/tldr-ipr). In any case, always include the full text of the license you chose in a text file to accompany your software code or hardware design files.

## Section 4 review activity

Reflect on the information you learnt in this section.

1. (true/false) Copyright automatically applies as soon as you express a creative work and “fix” it in any medium.
2. (true/false) The copyright owner of a piece of creative work is always the author.
3. (true/false) As long as my data analysis results are available online on a public webpage, others are allowed to use it.
4. The copyright on the Happy Birthday song lasted way beyond the death of the author. Does this incentivise authors to create? Does this fact add any motivation for you to create?
5. What are the benefits and drawbacks of the current copyright system? Do you agree that licenses such as the six Creative Commons licenses facilitate creativity and open science?